package com.example.news.repository;

import com.example.news.entity.News;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface NewsRepository extends JpaRepository<News, UUID> {

    Page<News> findByAcceptedFalse(Pageable pageable);

    Page<News> findByAcceptedTrue(Pageable pageable);

    @Modifying
    @Query("update News set accepted = true where id = :newsId")
    News updateById(UUID newsId);


    Optional<News> findByIdAndUserId(UUID id, UUID userId);

    Page<News> findByUserId(UUID userId,Pageable pageable);


}
