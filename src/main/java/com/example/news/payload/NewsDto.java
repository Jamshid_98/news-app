package com.example.news.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewsDto {

    private UUID id;

    private String content;

    private Boolean accepted;

    public NewsDto(String content) {
        this.content = content;
    }
}
