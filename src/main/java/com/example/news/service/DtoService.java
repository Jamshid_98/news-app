package com.example.news.service;

import com.example.news.entity.News;
import com.example.news.payload.NewsDto;
import org.springframework.stereotype.Service;

@Service
public class DtoService {

public NewsDto getNewsDto(News news){
return new NewsDto(
        news.getId(),
        news.getContent(),
        news.getAccepted()
);
}

}
