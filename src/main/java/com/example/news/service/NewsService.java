package com.example.news.service;

import com.example.news.entity.News;
import com.example.news.entity.User;
import com.example.news.payload.ApiResponse;
import com.example.news.payload.NewsDto;
import com.example.news.repository.NewsRepository;
import com.example.news.repository.UserRepository;
import com.example.news.utills.CommonUtills;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class NewsService {

    @Autowired
    UserRepository userRepository;


    @Autowired
    NewsRepository newsRepository;

    @Autowired
    DtoService dtoService;

    public ApiResponse adminNews(Integer page, Integer size){
        try {
            Page<News> newsList = newsRepository.findByAcceptedFalse(CommonUtills.getPageableByCreatedAtDesc(page,size));
            return new ApiResponse(true,"success",newsList.getContent().stream().map(item->dtoService.getNewsDto(item)).collect(Collectors.toList()),newsList.getTotalElements(),newsList.getTotalPages());
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"error");
        }
    }

    public ApiResponse acceptNews(UUID newsId){
        try {
                // TODO 1 chi find By id qilib set qilib keyin save qilish kerak
//                News acceptNewsList = newsRepository.updateById(newsId);

            News news = newsRepository.findById(newsId).orElse(null);

            if (news == null){
                new ApiResponse(false, "news not found");
            }
            news.setAccepted(true);

            newsRepository.save(news);

            return new ApiResponse(true,"news successfully updated");
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"error",null);
        }
    }

    public ApiResponse getUserNews(User user,Integer page, Integer size) {
        try {
                Page<News> getUserList = newsRepository.findByUserId(user.getId(),CommonUtills.getPageableByCreatedAtDesc(page,size));
                return new ApiResponse(true,"success",getUserList.getContent().stream().map(item->dtoService.getNewsDto(item)).collect(Collectors.toList()),getUserList.getTotalElements(),getUserList.getTotalPages());
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"error");
        }

    }

    public ApiResponse globalAllNews(User user,Integer page,Integer size){
        try {
                Page<News> globalNewsPage = newsRepository.findByAcceptedTrue(CommonUtills.getPageableByCreatedAtDesc(page, size));
                return new ApiResponse(true, "success",globalNewsPage.getContent().stream().map(item->dtoService.getNewsDto(item)).collect(Collectors.toList()),globalNewsPage.getTotalElements(),globalNewsPage.getTotalPages());
            }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"error");
        }
    }


    public ApiResponse saveOrEditNews(NewsDto newsDto,User user) {
        try {
            News news = new News();
            if (newsDto.getId()!=null) {
                news = newsRepository.findByIdAndUserId(newsDto.getId(), user.getId()).orElse(null);
            }
            if (news == null){
                return new ApiResponse(false, "news not found");
            }
                news.setId(newsDto.getId());
                news.setContent(newsDto.getContent());
                news.setAccepted(false);
                news.setUserId(user.getId());
                newsRepository.save(news);
                return new ApiResponse(true, newsDto.getId()!=null?"edited":"saved");
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"error");
        }

    }
}
