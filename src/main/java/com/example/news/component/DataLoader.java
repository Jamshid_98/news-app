package com.example.news.component;

import com.example.news.entity.Role;
import com.example.news.entity.User;
import com.example.news.entity.enums.RoleName;
import com.example.news.repository.RoleRepository;
import com.example.news.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by Sherlock on 09.04.2020.
 */

@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Value("${spring.datasource.initialization-mode}")
    String initialMode;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    String hibernateDdl;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... strings) throws Exception {
        if (initialMode.equals("always") && hibernateDdl.equals("create")) {

            roleRepository.save(new Role(RoleName.ROLE_ADMIN));
            roleRepository.save(new Role(RoleName.ROLE_USER));

            User user = new User(
                    23,
                    passwordEncoder.encode("0118"),
                    "Ismatov",
                    "Jamshid",
                    new ArrayList<Role>(Arrays.asList(roleRepository.findAllByName(RoleName.ROLE_ADMIN).orElseThrow(null))),
                    "jamshidismatov1998@gmail.com");
            userRepository.save(user);
        }
    }
}
