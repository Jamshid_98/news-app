package com.example.news.entity.enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER,
}
