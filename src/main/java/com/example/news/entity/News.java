package com.example.news.entity;

import com.example.news.entity.template.AbstractEntity;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
public class News extends AbstractEntity {

    @NotNull
    private String content;

    private Boolean accepted;

    private UUID userId;

    public News(String content) {
        this.content = content;
    }

    public News(String content, Boolean accepted, UUID userId) {
        this.content = content;
        this.accepted = accepted;
        this.userId = userId;
    }
}
