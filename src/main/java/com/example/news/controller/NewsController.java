package com.example.news.controller;
import com.example.news.entity.User;
import com.example.news.payload.ApiResponse;
import com.example.news.payload.NewsDto;
import com.example.news.security.CurrentUser;
import com.example.news.service.NewsService;
import com.example.news.utills.AppConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/news")
@CrossOrigin
public class NewsController {

    @Autowired
    NewsService newsService;

//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/adminNews")
    public HttpEntity<?> allFalse(@RequestParam(value = "page",defaultValue = AppConst.PAGE_DEFAULT_NUMBER)Integer page,
                                  @RequestParam(value = "size",defaultValue = AppConst.PAGE_DEFAULT_SIZE)Integer size){
        ApiResponse response = newsService.adminNews(page, size);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/acceptNews/{id}")
    public HttpEntity<?> acceptNews(@PathVariable(value = "id") UUID newsId){
        ApiResponse response = newsService.acceptNews(newsId);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

//    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/saveOrEdit")
    public HttpEntity<?> saveOrEdit(@Validated @RequestBody NewsDto dto,@CurrentUser User user){
        ApiResponse response = newsService.saveOrEditNews(dto,user);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/ownUserNews")
    public HttpEntity<?> getUserOwnNews(@CurrentUser User user,
                                        @RequestParam(value = "page",defaultValue = AppConst.PAGE_DEFAULT_NUMBER)Integer page,
                                        @RequestParam(value = "size",defaultValue = AppConst.PAGE_DEFAULT_SIZE)Integer size){
        ApiResponse response = newsService.getUserNews(user,page,size);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

//    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @GetMapping("/globalNews")
    public HttpEntity<?> getGlobalNews(@CurrentUser User user,
                                       @RequestParam(value = "page",defaultValue = AppConst.PAGE_DEFAULT_NUMBER)Integer page,
                                       @RequestParam(value = "size",defaultValue = AppConst.PAGE_DEFAULT_SIZE)Integer size){
        ApiResponse response = newsService.globalAllNews(user,page,size);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }
}
