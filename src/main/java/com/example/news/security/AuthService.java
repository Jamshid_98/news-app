package com.example.news.security;

import com.example.news.entity.Role;
import com.example.news.entity.User;
import com.example.news.entity.enums.RoleName;
import com.example.news.payload.ApiResponse;
import com.example.news.payload.ReqSignIn;
import com.example.news.payload.ReqSignUp;
import com.example.news.repository.RoleRepository;
import com.example.news.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Bunday telefon raqam topilmadi: " + email));
    }

    public UserDetails loadUserById(UUID userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("Bunday id topilmadi: " + userId));
    }

    public ApiResponse register(ReqSignUp reqSignUp) {
        Optional<User> optionalUser = userRepository.findByEmail(reqSignUp.getEmail());
        if (optionalUser.isPresent()) {
            return new ApiResponse(false, "Bunday foydalanuvchi ro'yxatdan o'tgan!");
        } else {
            userRepository.save(
                    new User(
                            reqSignUp.getAge(),
                            passwordEncoder.encode(reqSignUp.getPassword()),
                            reqSignUp.getLastName(),
                            reqSignUp.getFirstName(),
                            new ArrayList<Role>(Arrays.asList(roleRepository.findAllByName(RoleName.ROLE_ADMIN).orElseThrow(null))),
                            reqSignUp.getEmail()
                    ));
            return new ApiResponse(true, "Tizimdan muvaffaqiyatli ro'yxatdan o'tdingiz.");
        }
    }

    public ApiResponse login(ReqSignIn reqSignIn) {
        Optional<User> optionalUser = userRepository.findByEmail(reqSignIn.getEmail());

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (passwordEncoder.matches(reqSignIn.getPassword(), user.getPassword())) {
                return new ApiResponse(true, "Ma'lumotlar to'g'ri kiritildi!");
            } else {
                return new ApiResponse(false, "Parol xato kiritildi");
            }
        } else {
            return new ApiResponse(false, "Bunday foydalanuvchi ro'yhatdan o'tmagan!");
        }
    }
}
